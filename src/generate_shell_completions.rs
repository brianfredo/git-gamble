use crate::app::App;
use clap::CommandFactory;
use std::env;

pub fn generate_shell_completions<Generator>(shell_generator: Generator)
where
	Generator: Fn(&str, &mut clap::Command),
{
	let package_name = env!("CARGO_PKG_NAME");
	let mut command = App::command();

	shell_generator(package_name, &mut command)
}
