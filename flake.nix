{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    nix-cargo-integration = {
      url = "github:yusdacra/nix-cargo-integration";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

    # used only for retro compatibility with `nix-shell`
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs =
    { self
    , nixpkgs
    , nix-cargo-integration
    , flake-utils
    , flake-compat
    }:
    # uncomment to debug with `traceVal someValue` `traceSeq someValue` `traceValSeq someValue` `traceValSeqN 2 someValue`
    # with import ./nix/debug.nix { inherit nixpkgs; };
    (nix-cargo-integration.lib.makeOutputs {
      root = ./.;

      overrides = {
        crateOverrides = common: prevv: {
          git-gamble = prev: {
            nativeBuildInputs = (prev.nativeBuildInputs or [ ]) ++ [
              common.pkgs.installShellFiles
              common.pkgs.bash
            ];
            postInstall = ''
              ${prev.postInstall or ""}

              installShellCompletion --fish target/release/build/git-gamble-*/out/git-gamble.fish
              installShellCompletion --bash target/release/build/git-gamble-*/out/git-gamble.bash
              installShellCompletion --zsh target/release/build/git-gamble-*/out/_git-gamble

              PATH="$PATH:target/release/bin/" sh ./script/usage.sh > git-gamble.1
              installManPage git-gamble.1
            '';
          };
        };

        # run with `nix develop`
        shell = common: prev: import ./nix/shell.nix {
          pkgs = common.pkgs;
          default = prev;
        };
      };
    })
    //
    (flake-utils.lib.eachDefaultSystem (system: {
      formatter =
        let
          pkgs = import nixpkgs { inherit system; };
        in
        pkgs.nixpkgs-fmt;
    }))
    //
    ({
      overlay = import ./nix/overlay.nix;
    })
    //
    (flake-utils.lib.eachDefaultSystem (system: {
      overlays = import ./nix/overlay.nix;
    }));
}
