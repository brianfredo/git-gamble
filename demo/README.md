# Demo

<!-- markdownlint-disable MD033 -->

[`git-gamble`](https://gitlab.com/pinage404/git-gamble)'s demo to develop using TDD by doing babysteps

on a simple program

[`git-gamble`](https://gitlab.com/pinage404/git-gamble) works with all languages and tools and editors

for this example, i use `python` with `pytest` and `nano`

```sh
export GAMBLE_TEST_COMMAND='pytest --quiet'
```

note : for a simpler demo, test code and production code are mixed

---

## First TDD loop ➰

### First, 🔴 Red phase : write a test that fails for the good reason

<details>
<summary>

[![asciicast](https://asciinema.org/a/496949.svg)](https://asciinema.org/a/496949)

</summary>

```sh
nano test_hello.py
```

```python
def hello():
    pass

def test_say_hello_world():
    assert hello() == 'Hello world'
```

Then, gamble that the tests fail

```sh
git gamble --red
```

✔️ Committed

</details>

### Second, 🟢 Green phase : write the minimum code to pass the tests

<details>
<summary>

[![asciicast](https://asciinema.org/a/496951.svg)](https://asciinema.org/a/496951)

</summary>

```sh
nano test_hello.py
```

```python
def hello():
    return 'Hello word'
```

Then, gamble that the tests pass

```sh
git gamble --green
```

❌ Reverted

</details>

oh no ! i made a typo

Try again

<details>
<summary>

[![asciicast](https://asciinema.org/a/496952.svg)](https://asciinema.org/a/496952)

</summary>

```sh
nano test_hello.py
```

```python
def hello():
    return 'Hello world'
```

Gamble again that the tests pass

```sh
git gamble --green
```

✔️ Committed

</details>

Yeah !

### Third, 🔀 Refactor phase : i have nothing to refactor yet

---

Then 🔁 Repeat ➰

## Second TDD loop ➿

### 🔴 Red phase

<details>
<summary>

[![asciicast](https://asciinema.org/a/496957.svg)](https://asciinema.org/a/496957)

</summary>

```sh
nano test_hello.py
```

```python
def test_say_hello_name_given_a_name():
    assert hello('Jon') == 'Hello Jon'
```

```sh
git gamble --red
```

✔️ Committed

</details>

### 🟢 Green phase

<details>
<summary>

[![asciicast](https://asciinema.org/a/496959.svg)](https://asciinema.org/a/496959)

</summary>

```sh
nano test_hello.py
```

```python
def hello(arg=None):
    if arg:
        return f'Hello {arg}'
    return 'Hello world'
```

```sh
git gamble --green
```

✔️ Committed

</details>

### 🔀 Refactor loop ➰

#### 🔀 Refactor phase

i can simplify

<details>
<summary>

[![asciicast](https://asciinema.org/a/496961.svg)](https://asciinema.org/a/496961)

</summary>

```sh
nano test_hello.py
```

```python
def hello(arg='world'):
    return f'Hello {arg}'
```

```sh
git gamble --refactor
```

✔️ Committed

</details>

#### Still 🔀 Refactor phase : i have something else to refactor ➿

Better naming

<details>
<summary>

[![asciicast](https://asciinema.org/a/496963.svg)](https://asciinema.org/a/496963)

</summary>

```sh
nano test_hello.py
```

```python
def hello(name='world'):
    return f'Hello {name}'
```

```sh
git gamble --refactor
```

✔️ Committed

</details>

---

And so on... ➿

🔁 Repeat until you have tested all the rules, are satisfied and enougth confident
