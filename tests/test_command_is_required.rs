use std::io;
use tempfile::tempdir;
use test_case::test_case;

pub mod fixture;
use fixture::execute_cli;
use fixture::TestRepository;

#[test_log::test]
fn should_abort_without_test_command() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	execute_cli()
		.current_dir(repository.path())
		.env_remove("GAMBLE_TEST_COMMAND")
		.args(&["--pass", "--"])
		.assert()
		.failure();

	assert!(repository.is_dirty());
	assert!(repository.changes_remain_in_the_working_file());
	assert!(repository.has_no_new_commit());
	Ok(())
}

#[test_case("true" ; "simple command")]
#[test_case("sh -c \"test 'hello world' = 'hello world'\"" ; "complex command")]
#[test_log::test]
fn should_be_able_to_use_test_command_from_environment_variable_with(
	test_command: &str,
) -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	execute_cli()
		.current_dir(repository.path())
		.env("GAMBLE_TEST_COMMAND", test_command)
		.args(&["--pass"])
		.assert()
		.success();

	assert!(repository.is_clean());
	assert!(repository.changes_remain_in_the_working_file());
	assert!(repository.has_new_commit());
	Ok(())
}
