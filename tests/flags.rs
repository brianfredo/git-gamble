use std::io;
use tempfile::tempdir;
use test_log::test;

pub mod fixture;
use fixture::TestRepository;

#[test]
fn should_not_be_able_to_gamble_that_tests_pass_and_tests_fail_in_the_same_time() -> io::Result<()>
{
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	repository
		.execute_cli_with(&["--pass", "--fail", "--", "true"])
		.failure();

	assert!(repository.is_dirty());
	assert!(repository.changes_remain_in_the_working_file());
	assert!(repository.has_no_new_commit());
	Ok(())
}

#[test]
fn should_require_gambling() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	repository.execute_cli_with(&["--", "true"]).failure();

	assert!(repository.is_dirty());
	assert!(repository.changes_remain_in_the_working_file());
	assert!(repository.has_no_new_commit());
	Ok(())
}
