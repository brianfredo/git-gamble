use std::io;

pub mod fixture;
use fixture::assert_smoke_in_output_contains;
use fixture::execute_cli_with;

mod generate_shell_completions {
	use super::*;
	use test_log::test;

	#[test]
	fn for_fish() -> io::Result<()> {
		let command = execute_cli_with(&["generate-shell-completions", "fish"]).success();

		assert_smoke_in_output_contains(
			command,
			"complete -c git-gamble -n \"__fish_use_subcommand\" -s h -l help -d 'Print help information'",
		);
		Ok(())
	}

	#[test]
	fn for_bash() -> io::Result<()> {
		let command = execute_cli_with(&["generate-shell-completions", "bash"]).success();

		assert_smoke_in_output_contains(
			command,
			"complete -F _git-gamble -o bashdefault -o default git-gamble",
		);
		Ok(())
	}

	#[test]
	fn allow_to_call_subcommand_without_typing_the_entire_name() -> io::Result<()> {
		let command = execute_cli_with(&["generate", "fish"]).success();

		assert_smoke_in_output_contains(
			command,
			"complete -c git-gamble -n \"__fish_use_subcommand\" -s h -l help -d 'Print help information'",
		);
		Ok(())
	}
}
