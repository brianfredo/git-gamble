use speculoos::assert_that;
use std::io;
use tempfile::tempdir;

pub mod fixture;
use fixture::TestRepository;

#[test_log::test]
fn squash_commit() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;
	let commit_message = "squash! first commit";

	let command = repository
		.execute_cli_with_editor(&["--pass", "--squash", "@", "--", "true"], commit_message);

	command.success();
	assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_log::test]
fn squash_commit_with_commit_search() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;
	let commit_message = "squash! initial commit";

	let command = repository.execute_cli_with_editor(
		&["--pass", "--squash", ":/initial", "--", "true"],
		commit_message,
	);

	command.success();
	assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}
