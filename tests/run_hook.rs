use speculoos::assert_that;
use speculoos::path::PathAssertions;
use std::fs::create_dir_all;
use std::fs::rename as move_file;
#[cfg(unix)]
use std::fs::set_permissions;
use std::io;
#[cfg(unix)]
use std::os::unix::fs::PermissionsExt;
use tempfile::tempdir;

pub mod fixture;
use fixture::TestRepository;

#[test_log::test]
fn run_hook_when_hook_file_is_executable() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	let hook_output_directory = tempdir()?;
	let hook_output_path = hook_output_directory.path().join("hook_output");
	test_repository.add_hook(
		"pre-gamble",
		format!("touch {}", hook_output_path.display()).as_str(),
	)?;

	let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

	command.success();
	assert_that(&hook_output_path).exists();
	Ok(())
}

#[test_log::test]
fn run_hook_respecting_git_config_hooks_path() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	let hooks_path = test_repository.path().join("hooks");
	test_repository.set_config(&["core.hooksPath", hooks_path.to_str().unwrap()]);
	create_dir_all(&hooks_path)?;

	let hook_output_directory = tempdir()?;
	let hook_output_path = hook_output_directory.path().join("hook_output");
	test_repository.add_hook(
		"pre-gamble",
		format!("touch {}", hook_output_path.display()).as_str(),
	)?;
	let hook_path = hooks_path.join("pre-gamble");
	move_file(test_repository.hook_path("pre-gamble"), hook_path)?;

	let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

	command.success();
	assert_that(&hook_output_path).exists();
	Ok(())
}

#[test_log::test]
fn exit_when_a_hook_fail() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	test_repository.add_hook("pre-gamble", "false")?;

	let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

	command.failure();
	Ok(())
}

mod do_not_run_hook {
	use super::*;

	#[test_log::test]
	fn when_hook_file_does_not_exists() -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let hook_output_directory = tempdir()?;
		let hook_output_path = hook_output_directory.path().join("hook_output");

		let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

		command.success();
		assert_that(&hook_output_path).does_not_exist();
		Ok(())
	}

	#[cfg(unix)]
	#[test_log::test]
	fn when_hook_file_is_not_executable() -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let hook_output_directory = tempdir()?;
		let hook_output_path = hook_output_directory.path().join("hook_output");
		test_repository.add_hook(
			"pre-gamble",
			format!("touch >>{}", hook_output_path.display()).as_str(),
		)?;
		let hook_path = test_repository.path().join(".git/hooks/pre-gamble");
		// Owner: Read + Write
		const OWNER_READ_WRITE_PERMISSIONS: u32 = 0o600;
		set_permissions(
			hook_path,
			PermissionsExt::from_mode(OWNER_READ_WRITE_PERMISSIONS),
		)?;

		let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

		command.success();
		assert_that(&hook_output_path).does_not_exist();
		Ok(())
	}

	#[test_log::test]
	fn when_dry_run() -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let hook_output_directory = tempdir()?;
		let hook_output_path = hook_output_directory.path().join("hook_output");
		test_repository.add_hook(
			"pre-gamble",
			format!("touch {}", hook_output_path.display()).as_str(),
		)?;

		let command = test_repository.execute_cli_with(&["--pass", "--dry-run", "--", "true"]);

		command.success();
		assert_that(&hook_output_path).does_not_exist();
		Ok(())
	}

	#[test_log::test]
	fn when_no_verify() -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let hook_output_directory = tempdir()?;
		let hook_output_path = hook_output_directory.path().join("hook_output");
		test_repository.add_hook(
			"pre-gamble",
			format!("touch {}", hook_output_path.display()).as_str(),
		)?;

		let command = test_repository.execute_cli_with(&["--pass", "--no-verify", "--", "true"]);

		command.success();
		assert_that(&hook_output_path).does_not_exist();
		Ok(())
	}
}
