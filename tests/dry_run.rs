use std::io;
use tempfile::tempdir;
use test_case::test_case;

pub mod fixture;
use fixture::smoke_assertion::assert_smoke_committed;
use fixture::TestRepository;

#[test_case("--dry-run")]
#[test_case("-n")]
#[test_log::test]
fn dry_run(flag: &str) -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	let command = repository
		.execute_cli_with(&[flag, "--pass", "--", "true"])
		.success();

	assert!(repository.is_dirty());
	assert!(repository.changes_remain_in_the_working_file());
	assert!(repository.has_no_new_commit());
	assert_smoke_committed(command);
	Ok(())
}
