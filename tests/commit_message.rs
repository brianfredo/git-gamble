use speculoos::assert_that;
use std::io;
use tempfile::tempdir;

pub mod fixture;
use fixture::test_repository::THIRD_CONTENT;
use fixture::TestRepository;

mod commit_message {
	use super::*;
	use test_case::test_case;

	#[test_case("-m")]
	#[test_case("--message")]
	#[test_log::test]
	fn add_message_to_commit_when_tests_fail(flag: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let commit_message = "some message";
		let command = repository.execute_cli_with(&["--fail", flag, commit_message, "--", "false"]);

		command.success();
		assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
		Ok(())
	}

	#[test_case("-m")]
	#[test_case("--message")]
	#[test_log::test]
	fn add_message_to_commit_when_tests_pass(flag: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let commit_message = "some message";
		let command = repository.execute_cli_with(&["--pass", flag, commit_message, "--", "true"]);

		command.success();
		assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
		Ok(())
	}

	#[test_case("-m")]
	#[test_case("--message")]
	#[test_log::test]
	fn should_update_message_when_given_a_message_and_when_amending(flag: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		repository
			.execute_cli_with(&["--fail", "--", "false"])
			.success();

		repository.make_working_file_dirty_with(THIRD_CONTENT)?;

		let commit_message = "some message";
		repository
			.execute_cli_with(&["--fail", flag, commit_message, "--", "false"])
			.success();

		assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
		Ok(())
	}

	#[test_case("-m")]
	#[test_case("--message")]
	#[test_log::test]
	fn should_keep_message_when_amending_without_message_provided(flag: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let commit_message = "some message";
		repository
			.execute_cli_with(&["--fail", flag, commit_message, "--", "false"])
			.success();

		repository.make_working_file_dirty_with(THIRD_CONTENT)?;

		repository
			.execute_cli_with(&["--fail", "--", "false"])
			.success();

		assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
		Ok(())
	}
}
