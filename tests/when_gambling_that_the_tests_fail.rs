use std::io;
use std::str;
use tempfile::tempdir;

pub mod fixture;
use fixture::smoke_assertion::assert_smoke_committed;
use fixture::smoke_assertion::assert_smoke_reverted;
use fixture::TestRepository;
use fixture::THIRD_CONTENT;

mod when_gambling_that_the_tests_fail_that_should {
	use super::*;
	use test_case::test_case;

	#[test_case("--fail")]
	#[test_case("--red")]
	#[test_case("-r")]
	#[test_log::test]
	fn commit_when_tests_fail(flag: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let command = repository
			.execute_cli_with(&[flag, "--", "false"])
			.success();

		assert!(repository.is_clean());
		assert!(repository.changes_remain_in_the_working_file());
		assert!(repository.has_new_commit());
		assert!(repository.head_is_failing_ref());
		assert_smoke_committed(command);
		Ok(())
	}

	#[test_case("--fail")]
	#[test_case("--red")]
	#[test_case("-r")]
	#[test_log::test]
	fn revert_when_tests_pass(flag: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let command = repository.execute_cli_with(&[flag, "--", "true"]).success();

		assert!(repository.is_clean());
		assert!(repository.changes_reverted_in_the_working_file());
		assert!(repository.has_no_new_commit());
		assert_smoke_reverted(command);
		Ok(())
	}

	#[test_case("--fail")]
	#[test_case("--red")]
	#[test_case("-r")]
	#[test_log::test]
	fn amend_commit_when_tests_fail_and_the_last_test_failed(flag: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		repository
			.execute_cli_with(&[flag, "--", "false"])
			.success();
		let previous_commit_hash = repository.get_commit_hash_for("@");

		repository.make_working_file_dirty_with(THIRD_CONTENT)?;

		repository
			.execute_cli_with(&[flag, "--", "false"])
			.success();

		assert!(repository.is_clean());
		assert!(repository.working_file_is_equal_to(THIRD_CONTENT));
		assert!(repository.has_new_commit());
		assert!(repository.head_is_failing_ref());
		assert!(repository.head_is_different_from(previous_commit_hash));
		Ok(())
	}
}
