#!/usr/bin/env sh

REAL_PATH=$(realpath "$0")
DIRECTORY_PATH=$(dirname "${REAL_PATH}")
HOOKS_PATH=$(dirname "${DIRECTORY_PATH}")
FIXTURE_PATH="${DIRECTORY_PATH}/fixture"
GIT_OUTPUT="${FIXTURE_PATH}/git_output.txt"

tearDown() {
    rm --force "${GIT_OUTPUT}"
}

export PATH="${FIXTURE_PATH}:${PATH}"
export GAMBLE_TEST_COMMAND="echo 'test executed' >>${GIT_OUTPUT}"

test_do_nothing_when_test_fail() {
    "${HOOKS_PATH}/post-gamble.real_time_collaboration.sample.sh" pass fail

    assertTrue "git command shouldn't be executed" "[ ! -f '${GIT_OUTPUT}' ]"
}

test_do_nothing_when_gambled_that_test_fail() {
    "${HOOKS_PATH}/post-gamble.real_time_collaboration.sample.sh" fail pass

    assertTrue "git command shouldn't be executed" "[ ! -f '${GIT_OUTPUT}' ]"
}

test_pull_when_test_pass() {
    "${HOOKS_PATH}/post-gamble.real_time_collaboration.sample.sh" pass pass

    assertContains "git pull --rebase" "$(cat ${GIT_OUTPUT})"
}

test_pull_then_execute_test_when_test_pass() {
    "${HOOKS_PATH}/post-gamble.real_time_collaboration.sample.sh" pass pass

    assertContains "test executed" "$(cat ${GIT_OUTPUT})"
}

test_pull_then_execute_test_then_push_when_test_pass() {
    "${HOOKS_PATH}/post-gamble.real_time_collaboration.sample.sh" pass pass

    assertContains "git push" "$(cat ${GIT_OUTPUT})"
}

test_pull_then_execute_test_then_not_push_when_test_fail_after_pull() {
    GAMBLE_TEST_COMMAND="false" "${HOOKS_PATH}/post-gamble.real_time_collaboration.sample.sh" pass pass

    assertNotContains "git push" "$(cat ${GIT_OUTPUT})"
}

. shunit2
